
import { SkComponentImpl } from '../../../sk-core/src/impl/sk-component-impl.js';
import { SK_RENDER_EVT, SkRenderEvent } from "../../../sk-core/src/event/sk-render-event.js";


export class SkDialogImpl extends SkComponentImpl {

    get suffix() {
        return 'dialog';
    }

    renderTitle() {
        if (this.comp.title) {
            this.headerEl.style.display = 'block';
            this.titleEl.innerHTML = this.comp.title;
        } else {
            this.titleEl.style.display = 'none';
        }
    }

    dumpState() {
        return {
            'contents': this.comp.contentsState
        }
    }

    unmountStyles() {
        super.unmountStyles();
        if (this.styles && this.styles.length > 0) {
            for (let style of this.styles) {
                let path = this.comp.configEl.styles[style];
                let link = document.body.querySelector(`link[href="${path}"]`);
                if (link !== null) {
                    document.body.removeChild(link);
                }
            }
        }
    }

    restoreState(state) {
        this.body.innerHTML = '';
        this.body.insertAdjacentHTML('beforeend', state.contentsState);
    }

    cleanFooterTplCache() {
        let oldTpl = document.getElementById(this.comp.constructor.name + 'FooterTpl');
        if (oldTpl !== null) {
            if (typeof oldTpl.remove === 'function') {
                oldTpl.remove();
            } else {
                if (oldTpl.parentElement) {
                    oldTpl.parentElement.removeChild(oldTpl);
                }
            }
        }
    }

    async renderFooter() {
        this.cleanFooterTplCache();
        let renderFooterContents = function(tpl) {
            let el;
            if (tpl.content) {
                el = this.comp.renderer.prepareTemplate(tpl);
            } else {
                el = tpl.firstElementChild;
            }

            this.footerEl.innerHTML = '';
            if (this.isInIE()) {
                this.footerEl.appendChild(el);
            } else {
                this.footerEl.append(el);
            }
            this.bindActions(this.footerEl);
        }.bind(this);
        let tpl = this.comp.renderer.findTemplateEl(this.comp.constructor.name + 'FooterTpl', this.dialog); // try to find overriden template
        if (! tpl) {
            let tplLoaded = this.comp.renderer.mountTemplate(
                this.tplFooterPath, this.comp.constructor.name + 'FooterTpl',
                this.comp, {
                    Ok: this.comp.locale.tr('Ok'),
                    Cancel: this.comp.locale.tr('Cancel')
                });
            tplLoaded.then(tpl => {
                renderFooterContents(tpl);
            });
        } else {
            renderFooterContents(tpl);
        }
    }

    reattachStylesToBody() {
        let styles = Object.keys(this.skTheme.styles);
        if (styles) {
            for (let style of styles) {
                this.attachStyleByPath(this.skTheme.styles[style], document.body);
            }
        }
    }

    remountToBody(dialog) {
        this.reattachStylesToBody();
        if (this.isInIE()) {
            this.dialog = document.body.appendChild(dialog);
            this.comp.bindAutoRender(this.dialog);
            this.comp.setupConnections();
            this.bindActions(this.dialog);
        } else {
            document.body.append(dialog);
            this.dialog = dialog;
        }
    }

    polyfillNativeDialog(dialog) {
        if (this.comp.dialogTn === 'dialog') {
            if (typeof this.dialog.showModal !== "function") {
                if (!window.dialogPolyfill) {
                    this.logger.warn('you trying to polyfill native dialog element, but polyfill was not loaded');
                    return false;
                }
                this.remountToBody(dialog);
                window.dialogPolyfill.registerDialog(this.dialog);
                this.dialogPolyfilled = true;
            }
        } else {
            this.comp.style.position = 'fixed';
            this.comp.style.zIndex = 500;
        }
        if (this.comp.hasAttribute('to-body')) {
            this.remountToBody(this.dialog);
        }
    }


    getScrollTop() {
        if (typeof pageYOffset !== 'undefined') {
            return pageYOffset;
        } else {
            let body = document.body;
            let document = document.documentElement;
            document = (document.clientHeight) ? document : body;
            return document.scrollTop;
        }
    }

    fixDialogPosition() {
        let box = this.dialog.getBoundingClientRect();
        let winWidth = document.body.clientWidth || document.clientWidth;
        let winHeight = document.body.clientHeight || document.clientHeight;
        this.dialog.style.position = 'fixed';
        let dialogLeft = this.dialog.style.right = (((winWidth - box.width) / 2))
        dialogLeft = dialogLeft > 0 ? dialogLeft : 0
        this.dialog.style.left = (dialogLeft).toString() + 'px';
        let dialogTop = (((winHeight - box.height) / 2));
        dialogTop = dialogTop > 0 ? dialogTop : 0;
        this.dialog.style.top = (dialogTop).toString() + 'px';
        this.dialog.style.margin = 0;
        this.dialog.style.padding = 0;
    }

    open() {
        this.dialog.style.setProperty('display', 'block', 'important');
        if (typeof this.dialog.showModal === "function") {
            if (! this.dialog.hasAttribute('open')) {
                this.dialog.showModal();
            } else {
                this.logger.warn('trying to open dialog with open attr');
            }
        } else {
            this.logger.warn('native dialog not supported by this browser');
        }
        if (this.isInIE() && ! this.dialog.hasAttribute('open')) { // assume dialog can be not only native like
            this.comp.setAttribute('open', 'true');
        } else {
            this.comp.setAttribute('open', '');
        }
        if (this.comp.fixPosHandler) {
            this.dialog.removeEventListener(SK_RENDER_EVT, this.fixPosHandler);
        }
        this.comp.fixPosHandler = function(event) {
            this.fixDialogPosition();
        }.bind(this);
        this.dialog.addEventListener(SK_RENDER_EVT, this.comp.fixPosHandler);
        if (this.comp.resizeHandler) {
            window.removeEventListener(SK_RENDER_EVT, this.comp.fixPosHandler);
        }
        this.comp.resizeHandler = function(event) {
            this.fixDialogPosition();
        }.bind(this);
        window.addEventListener('resize', this.comp.resizeHandler);
        if (this.comp.dialogTn !== 'dialog') {
            this.fixDialogPosition();
        }
    }

    close() {
        this.dialog.style.setProperty('display', 'none', 'important');
        if (typeof this.dialog.close === "function" && this.dialog.hasAttribute('open')) {
            this.dialog.close();
        } else {
            this.logger.warn('native dialog not supported by this browser');
        }
        if (this.comp.hasAttribute('open')) {
            this.comp.removeAttribute('open');
        }
    }

    doRender() {
        let id = this.getOrGenId();
        this.beforeRendered();
        let el = this.comp.renderer.prepareTemplate(this.comp.tpl);
        let dialog = this.comp.renderer.createEl(this.comp.dialogTn);
        dialog.setAttribute('id', id + 'Dialog');
        let rendered;
        if (this.comp.renderer.variableRender && this.comp.renderer.variableRender !== 'false') {
            rendered = this.comp.renderer.renderMustacheVars(el, {
                id: id,
                themePath: this.themePath
            });
            dialog.insertAdjacentHTML('beforeend', rendered);
        } else {
            dialog.innerHTML = '';
            dialog.appendChild(el);
        }
        for (let template of this.tplClones) {
            dialog.appendChild(template);
        }
        if (this.comp.hasAttribute('to-body')) {
            this.dialog = document.body.appendChild(dialog);
        } else {
            this.comp.el.innerHTML = '';
            this.dialog = this.comp.el.appendChild(dialog);
            this.indexMountedStyles();
        }
        this.afterRendered();
        if (this.comp.hasAttribute('open') && ! this.comp.implRenderTimest) {
            this.open();
        }
        this.comp.dispatchEvent(new CustomEvent('skafterrendered'));
        this.comp.bindAutoRender();
        this.comp.setupConnections();
        this.comp.callPluginHook('onRenderEnd');
        this.comp.implRenderTimest = Date.now();
        this.comp.removeFromRendering();
        this.comp.dispatchEvent(new CustomEvent('rendered', { bubbles: true, composed: true }));
        this.comp.dispatchEvent(new SkRenderEvent({ bubbles: true, composed: true }));
        if (this.comp.renderDeferred) {
            this.comp.renderDeferred.resolve(this.comp);
        }
    }

    renderImpl() {
        let ownTemplates = this.comp.querySelectorAll('template');
        this.tplClones = [];
        for (let template of ownTemplates) {
            this.tplClones.push(template.cloneNode(true));
        }
        let themeLoaded = this.initTheme();
        themeLoaded.finally(() => { // we just need to try loading styles
            this.comp.tpl = this.comp.renderer.findTemplateEl(this.cachedTplId, this.comp); // try to find overriden template
            if (! this.comp.tpl) {
                const loadTpl = async () => {
                    this.comp.tpl = await this.comp.renderer.mountTemplate(
                        this.tplPath, this.cachedTplId,
                        this.comp, {
                            themePath: this.themePath
                        });
                }
                loadTpl().then(() => {
                    this.doRender();
                });
            } else {
                this.doRender();
            }
        });

    }

    setTitle(title) {
        this.titleEl.innerHTML = title;
    }
}