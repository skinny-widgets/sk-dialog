
import { SkElement } from '../../sk-core/src/sk-element.js';

export class SkDialog extends SkElement {

    get cnSuffix() {
        return 'dialog';
    }

    get impl() {
        if (!this._impl) {
            this.initImpl();
        }
        return this._impl;
    }

    open() {
        this.impl.open();
    }

    close() {
        this.impl.close();
    }

    toggle() {
        if (this.open) {
            this.impl.close();
        } else {
            this.impl.open();
        }
    }

    set impl(impl) {
        this._impl = impl;
    }

    get dialogTn() {
        return this.confValOrDefault('dialog-tn', 'dialog');
    }

    set dialogTn(dialogTn) {
        return this.setAttribute('dialog-tn', dialogTn);
    }

    get type() {
        return this.getAttribute('type');
    }

    oncancel() {
        this.close();
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if (name === 'open' && this.implRenderTimest) {
            if (newValue !== null && (! oldValue)) {
                if (this.impl && ! this.impl.open) {
                    this.open();
                }
            } else {
                if (newValue === null) {
                    this.close();
                }
            }
        }
        if (name === 'title' && this.implRenderTimest) {
            this.impl.setTitle(newValue);
        }
        this.callPluginHook('onAttrChange', name, oldValue, newValue);
    }

    static get observedAttributes() {
        return ['open', 'title'];
    }
}
